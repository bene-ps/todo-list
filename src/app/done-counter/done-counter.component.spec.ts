import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoneCounterComponent } from './done-counter.component';

describe('DoneCounterComponent', () => {
  let component: DoneCounterComponent;
  let fixture: ComponentFixture<DoneCounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoneCounterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoneCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
