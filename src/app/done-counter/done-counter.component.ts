import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {TodoItemsService} from '../todo-items.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-done-counter',
  templateUrl: './done-counter.component.html',
  styleUrls: ['./done-counter.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DoneCounterComponent implements OnInit {

  get numberOfDoneItems$(): Observable<number> {
    return this.todoItemsService.numberOfDoneItems$;
  }

  get numberOfItems$(): Observable<number> {
    return this.todoItemsService.numberOfItems$;
  }

  constructor(private todoItemsService: TodoItemsService) { }

  ngOnInit(): void {
  }

}
