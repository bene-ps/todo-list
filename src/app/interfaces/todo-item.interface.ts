export interface TodoItemInterface {
  id: number;
  title: string;
  description: string;
  done: boolean;
}
