import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {TodoItemInterface} from './interfaces/todo-item.interface';
import {filter, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoItemsService {
  todoListBS: BehaviorSubject<TodoItemInterface[]> = new BehaviorSubject<TodoItemInterface[]>([]);

  get numberOfDoneItems$(): Observable<number> {
    return this.todoList$.pipe(
      map((items) => items.filter(({done}) => done).length),
    );
  }

  get numberOfItems$(): Observable<number> {
    return this.todoList$.pipe(
      map((items) => items.length)
    );
  }

  get todoList$(): Observable<TodoItemInterface[]> {
    return this.todoListBS.asObservable();
  }

  constructor() { }

  addItem(item: TodoItemInterface): void {
    const currentList = this.todoListBS.value;
    this.todoListBS.next([...currentList, {...item, id: currentList.length}]);
  }

  editItem(itemToUpdate: TodoItemInterface): void {
    const currentList = this.todoListBS.value;
    const currentItemIndex = currentList.findIndex((item) => {
      return item.id === itemToUpdate.id;
    });
    if (currentItemIndex >= 0) {
      currentList[currentItemIndex] = itemToUpdate;
      this.todoListBS.next(currentList);
    }
  }
}
