import {EventEmitter, ChangeDetectionStrategy, Component, Input, OnInit, Output} from '@angular/core';
import {TodoItemInterface} from '../interfaces/todo-item.interface';
import {TodoItemsService} from '../todo-items.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit {
  @Input()
  item: TodoItemInterface;
  @Output()
  itemUpdate = new EventEmitter<TodoItemInterface>();

  constructor() { }

  ngOnInit(): void {
  }

  handleDoneChange(isDone: boolean): void {
    const item = this.item;
    this.itemUpdate.emit({...item, done: isDone});
  }
}
