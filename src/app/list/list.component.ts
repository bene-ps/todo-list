import { Component, OnInit } from '@angular/core';
import {TodoItemsService} from '../todo-items.service';
import {Observable} from 'rxjs';
import {TodoItemInterface} from '../interfaces/todo-item.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  get items$(): Observable<TodoItemInterface[]> {
    return this.todoItemsService.todoList$;
  }

  constructor(private todoItemsService: TodoItemsService) { }

  ngOnInit(): void {
    this.todoItemsService.todoList$.subscribe(console.log);
  }

  handleItemUpdate(item: TodoItemInterface): void {
    this.todoItemsService.editItem(item);
  }
}
