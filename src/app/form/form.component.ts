import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TodoItemsService} from '../todo-items.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private todoItemsService: TodoItemsService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      title: [''],
      description: [''],
    });
  }

  handleSubmit(): void {
    this.todoItemsService.addItem(this.form.value);
    this.form.reset();
  }
}
